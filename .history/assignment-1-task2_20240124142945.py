# task 2

fruits = ["Apple: 0.99", "Banana: 1.25", "Orange: 1.50"]
sentences = []

for i in range(len(fruits)):
    item, price = fruits[i].split(': ')
    sentences.append(f'Price for {item} is {price}')
print(sentences)